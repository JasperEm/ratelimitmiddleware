<?php
use RateLimitMiddleware\Factory\RateLimitMiddlewareFactory;
use RateLimitMiddleware\RateLimitMiddleware;

return [
    'rate_limit' => [
        'max_requests' => 100,
        'reset_time' => 100,
    ],
    'dependencies' => [
        'factories' => [
            RateLimitMiddleware::class => RateLimitMiddlewareFactory::class
        ]
    ]
];
