<?php

/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 02.06.17
 * Time: 05:42
 */
namespace RateLimitMiddleware\Storage;

use RateLimitMiddleware\Interfaces\StorageInterface;

/**
 * Class ApcStorage
 * @package App\Middleware\Storage
 */
class ApcStorage implements StorageInterface
{
    /**
     * @param string $key
     * @return string
     */
    public function get(string $key)
    {
        if (!apcu_exists($key)) {
            return null;
        }

        return apcu_fetch($key);
    }

    /**
     * @param string $key
     * @param string $value
     * @return bool
     */
    public function set(string $key, $value): bool
    {
        return (bool)apcu_store($key, $value);
    }
}
