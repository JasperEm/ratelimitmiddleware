<?php

namespace RateLimitMiddleware\Factory;

use RateLimitMiddleware\RateLimitMiddleware;
use RateLimitMiddleware\Storage\ApcStorage;
use Interop\Container\ContainerInterface;

/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 27.05.17
 * Time: 00:20
 */
class RateLimitMiddlewareFactory
{
    public function __invoke(ContainerInterface $container): RateLimitMiddleware
    {
        $config = $container->get('config')['rate_limit'];
        $rateLimitMiddleware = new RateLimitMiddleware(new ApcStorage(), $config['max_requests']);
        $rateLimitMiddleware->setResetTime($config['reset_time']);

        return $rateLimitMiddleware;
    }
}
