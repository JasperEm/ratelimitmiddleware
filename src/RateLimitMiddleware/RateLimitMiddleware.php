<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 27.05.17
 * Time: 00:20
 */

namespace RateLimitMiddleware;

use RateLimitMiddleware\Storage\ApcStorage;
use Fig\Http\Message\StatusCodeInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class RateLimitMiddleware
 * @category RateLimit
 * @package App\Middleware
 */
class RateLimitMiddleware implements MiddlewareInterface
{

    const HEADER_LIMIT = 'X-Rate-Limit-Limit';
    const HEADER_RESET = 'X-Rate-Limit-Reset';
    const HEADER_REMAINING = 'X-Rate-Limit-Remaining';

    /**
     * @var ApcStorage
     */
    protected $storage;

    /**
     * @var string
     */
    protected $prefix = 'rate-limit-middleware';

    /**
     * @var int
     */
    protected $maxRequests;

    /**
     * @var int
     */
    protected $resetTime;

    /**
     * RateLimitMiddleware constructor.
     * @param ApcStorage $storage
     * @param int $maxRequests
     */
    public function __construct(ApcStorage $storage, $maxRequests)
    {
        $this->storage = $storage;
        $this->maxRequests = $maxRequests;
    }


    /**
     * Process an incoming server request and return a response, optionally delegating
     * to the next middleware component to create the response.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $remoteIp = $request->getServerParams()['REMOTE_ADDR'];

        $ip = $this->getStorage()->get($remoteIp);

        if (empty($ip) || !is_array($ip) || !(isset($ip['remaining']) && isset($ip['created']))) {
            $ip = [
                'remaining' => $this->getMaxRequests() - 1,
                'created' => time(),
            ];
        }

        $remaining = $ip['remaining'];
        $created = $ip['created'];

        if ($created === 0) {
            $created = time();
        } else {
            $remaining--;
        }

        $resetIn = ($created + $this->getResetTime()) - time();

        if ($resetIn <= 0) {
            $remaining = $this->getMaxRequests();
            $created = time();
            $resetIn = $this->getResetTime();
        }

        $ip = [
            'remaining' => $remaining,
            'created' => $created
        ];

        $this->getStorage()->set($remoteIp, $ip);

        if ($remaining < 0) {
            return new JsonResponse(
                [
                    'message' => 'rate limit exited'
                ],
                StatusCodeInterface::STATUS_TOO_MANY_REQUESTS
            );
        }

        return $delegate->process($request)
            ->withHeader(self::HEADER_REMAINING, $remaining)
            ->withAddedHeader(self::HEADER_LIMIT, $this->getMaxRequests())
            ->withAddedHeader(self::HEADER_RESET, $resetIn);
    }

    /**
     * @return ApcStorage
     */
    public function getStorage(): ApcStorage
    {
        return $this->storage;
    }

    /**
     * @param ApcStorage $storage
     * @return RateLimitMiddleware
     */
    public function setStorage(ApcStorage $storage): RateLimitMiddleware
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     * @return RateLimitMiddleware
     */
    public function setPrefix(string $prefix): RateLimitMiddleware
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxRequests(): int
    {
        return $this->maxRequests;
    }

    /**
     * @param int $maxRequests
     * @return RateLimitMiddleware
     */
    public function setMaxRequests(int $maxRequests): RateLimitMiddleware
    {
        $this->maxRequests = $maxRequests;
        return $this;
    }

    /**
     * @return int
     */
    public function getResetTime(): int
    {
        return $this->resetTime;
    }

    /**
     * @param int $resetTime
     * @return RateLimitMiddleware
     */
    public function setResetTime(int $resetTime): RateLimitMiddleware
    {
        $this->resetTime = $resetTime;
        return $this;
    }
}
