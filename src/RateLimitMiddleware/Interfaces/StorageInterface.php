<?php

/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 02.06.17
 * Time: 07:57
 */

namespace RateLimitMiddleware\Interfaces;

/**
 * Interface StorageInterface
 * @package App\Middleware\Interfaces
 */
interface StorageInterface
{
    /**
     * @param string $key
     * @param $value
     * @return bool
     */
    public function set(string $key, $value): bool;

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key);
}
